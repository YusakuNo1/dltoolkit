//
//  DLNetworkManager.swift
//  NowTapped
//
//  Created by David Wu on 1/03/2015.
//  Copyright (c) 2015 NowTapped. All rights reserved.
//

import Foundation


public class DLNetworkManager {
    
    public static let kMIMETypeJSON = "application/json"
    
    public class DLNetworkSession {
        private var _session: NSURLSession!
        public var session: NSURLSession {
            get {
                if (_session == nil) {
                    let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
                    sessionConfig.allowsCellularAccess = true
                    sessionConfig.HTTPAdditionalHeaders = [
                        "Accept": DLNetworkManager.kMIMETypeJSON,
                        "Content-Type": DLNetworkManager.kMIMETypeJSON,
                    ]
                    _session = NSURLSession(configuration: sessionConfig)
                }
                return _session
            }
        }
        
        public func downloadImage(urlString: String, callback: ((error: DLApiError?, image: UIImage?) -> Void)?) {
            downloadData(urlString, callback: { (error, data) -> Void in
                if (error != nil) {
                    callback?(error: error, image: nil)
                }
                else {
                    let image = UIImage(data: data!)
                    callback?(error: nil, image: image)
                }
            })
        }
        
        // T is only available
        public func downloadJson<T>(urlString: String, callback: ((error: DLApiError?, result: T?) -> Void)?) {
            downloadData(urlString, callback: { (error, data) -> Void in
                if (error != nil) {
                    callback?(error: error, result: nil)
                }
                else {
                    let result = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.allZeros, error: nil) as? T
                    if (result == nil) {
                        callback?(error: DLApiError(title: nil, message: "Failed to parse JSON format"), result: nil)
                    }
                    else {
                        callback?(error: nil, result: result!)
                    }
                }
            })
        }

        
        // MARK: - Private
        
        private func downloadData(urlString: String, callback: ((error: DLApiError?, data: NSData?) -> Void)?) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                let url = NSURL(string: urlString)
                var request = NSMutableURLRequest(URL: url!)
                request.HTTPMethod = "GET"
                let task = self.session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
                    DLUtils.runOnMainThread {
                        if (error != nil) {
                            callback?(error: DLApiError(error: error!), data: nil)
                        }
                        else if (data == nil) {
                            callback?(error: DLApiError(title: nil, message: "Download failed."), data: nil)
                        }
                        else {
                            callback?(error: nil, data: data)
                        }
                    }
                })
                task.resume()
            })
        }
    }
    
    public class var defaultSession: DLNetworkSession {
        struct Static {
            static let instance = DLNetworkSession()
        }
        return Static.instance
    }

    public class func createSession() -> DLNetworkSession {
        return DLNetworkSession()
    }
    
}
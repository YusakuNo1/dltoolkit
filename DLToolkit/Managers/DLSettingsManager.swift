//
//  DLSettingsManager.swift
//  NowTapped
//
//  Created by David Wu on 15/02/2015.
//  Copyright (c) 2015 NowTapped. All rights reserved.
//

import Foundation


public class DLSettingsManager {
    
    // MARK: - Private
    
    public class func saveSetting(value: NSObject, key: String) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setObject(value, forKey: key)
        userDefaults.synchronize()
    }

}
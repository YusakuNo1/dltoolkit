//
//  DLApiError.swift
//  NowTapped
//
//  Created by David Wu on 14/02/2015.
//  Copyright (c) 2015 None. All rights reserved.
//

import Foundation


public class DLApiError {
    public let title: String?
    public let message: String
    public let params: Dictionary<String, AnyObject>?
    
    public init(title: String?, message: String, params: Dictionary<String, AnyObject>? = nil) {
        self.title = title
        self.message = message
        self.params = params
    }

    public init(error: NSError) {
        self.title = nil
        self.message = error.localizedDescription
        self.params = nil
    }
}


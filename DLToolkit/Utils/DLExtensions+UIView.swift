//
//  DLExtensions.swift
//  NowTapped
//
//  Created by David Wu on 14/02/2015.
//  Copyright (c) 2015 None. All rights reserved.
//

import UIKit

public extension UIView {
    
    public func findViewInHierarchyWithClass(targetClass: AnyClass) -> UIView? {
        if (self.isKindOfClass(targetClass)) {
            return self
        }
        
        if (self.superview != nil) {
            return self.superview!.findViewInHierarchyWithClass(targetClass)
        }
        
        return nil
    }
    
}
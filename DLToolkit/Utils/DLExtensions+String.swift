//
//  DLExtensions+NSString.swift
//  DLToolkit
//
//  Created by David Wu on 6/04/2015.
//  Copyright (c) 2015 David Wu. All rights reserved.
//

import Foundation

public extension String {
    
    public func isEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluateWithObject(self)
    }
    
    public func getSlug() -> String {
        var array = Array<Character>()
        for c in self {
            if (("a" <= c && c <= "z") || ("A" <= c && c <= "Z") || ("0" <= c && c <= "9")) {
                array.append(c)
            }
        }
        return String(array).lowercaseString
    }
    
    public func trim() -> String {
        let length = self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)
        
        var startIndex = self.startIndex
        while (startIndex < self.endIndex && self[startIndex] == " ") {
            startIndex++
        }
        
        var lastIndex = self.endIndex
        lastIndex--
        while (lastIndex > startIndex && self[lastIndex] == " ") {
            lastIndex--
        }
        
        return self.substringWithRange(startIndex...lastIndex)
    }
    
}

//
//  DLUtils.swift
//  NowTapped
//
//  Created by David Wu on 14/02/2015.
//  Copyright (c) 2015 None. All rights reserved.
//

import Foundation


public class DLUtils {
    
    public class func getAppVersion() -> String {
        let appVersion = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as? String
        return appVersion == nil ? "" : appVersion!
    }
    
    public class func getAppVersionBuildNo() -> String {
        let buildNo = NSBundle.mainBundle().objectForInfoDictionaryKey(kCFBundleVersionKey as String) as? String
        return buildNo == nil ? "" : buildNo!
    }

    public class func isZero(value: CGFloat) -> Bool {
        let kMinValue: CGFloat = 0.0001
        return (-kMinValue < value) && (value < kMinValue)
    }
    
    public class func isEqual(value1: CGFloat, value2: CGFloat) -> Bool {
        return isZero(value1 - value2)
    }
    
    public class func colorFromHex(v: Int) -> UIColor {
        let r = (CGFloat)((v & 0xFF0000) >> 16)
        let g = (CGFloat)((v & 0xFF00) >> 8)
        let b = (CGFloat)((v & 0xFF))
        return UIColor(red: (r/255.0), green: (g/255.0), blue: (b/255.0), alpha: 1)
    }
    
    public class func loadStringFromFile(fileName: String) -> String? {
        let path = NSBundle.mainBundle().pathForResource(fileName, ofType: nil)
        if (path != nil) {
            return String(contentsOfFile: path!, encoding: NSUTF8StringEncoding, error: nil)
        }
        return nil
    }
    
    public class func runOnMainThread(callback: Void -> Void) {
        if (NSThread.isMainThread()) {
            callback()
        }
        else {
            dispatch_async(dispatch_get_main_queue(), callback)
        }
    }
    
    public class func runOnBackgroundThread(callback: Void -> Void) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), callback)
    }
    
}
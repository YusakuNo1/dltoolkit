//
//  DLExtensions+UIImageView.swift
//  DLToolkit
//
//  Created by David Wu on 6/04/2015.
//  Copyright (c) 2015 David Wu. All rights reserved.
//

import UIKit

public extension UIImageView {
    
    public func loadImage(urlString: String?) {
        if (urlString == nil) {
            return
        }
        
        DLNetworkManager.defaultSession.downloadImage(urlString!, callback: { (error, image) -> Void in
            if (error == nil) {
                self.image = image
            }
            else {
                NSLog("Failed to download image \(urlString)")
            }
        })
    }
    
}

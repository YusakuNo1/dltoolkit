//
//  DLExtensions+UIImage.swift
//  NowTapped
//
//  Created by David Wu on 1/04/2015.
//  Copyright (c) 2015 NowTapped. All rights reserved.
//

import UIKit

public extension UIImage {

    public func scale(newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }

}

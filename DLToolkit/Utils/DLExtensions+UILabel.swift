//
//  DLExtensions+UILabel.swift
//  DLToolkit
//
//  Created by David Wu on 6/04/2015.
//  Copyright (c) 2015 David Wu. All rights reserved.
//

import UIKit

public extension UILabel {
    
    public func replaceTextsWithIcons(texts: Array<String>, iconFiles: Array<String>) {
        func replace(text: String, iconFile: String) {
            let iconAttachment = NSTextAttachment();
            iconAttachment.image = UIImage(named: iconFile)
            let iconAttributedString = NSAttributedString(attachment: iconAttachment)
            
            let originText = self.text!
            let attributedText = NSMutableAttributedString(string: originText)
            attributedText.replaceText(text, withAttributedString: NSMutableAttributedString(attributedString: iconAttributedString))
            self.attributedText = attributedText
        }
        
        for (var i=0; i<texts.count && i<iconFiles.count; i++) {
            replace(texts[i], iconFiles[i])
        }
    }
    
}

//
//  DLDeviceUtils.swift
//  NowTapped
//
//  Created by David Wu on 14/02/2015.
//  Copyright (c) 2015 None. All rights reserved.
//

import Foundation


public class DLDeviceUtils {
    
    public class func screenSize() -> CGSize {
        return UIScreen.mainScreen().bounds.size
    }
    
    public class func screenMaxLength() -> CGFloat {
        return max(screenSize().width, screenSize().height)
    }
    
    public class func screenMinLength() -> CGFloat {
        return min(screenSize().width, screenSize().height)
    }

    public class func isIPad() -> Bool {
        return getUIUserInterfaceIdiom() == .Pad
    }
    
    public class func isIPhone() -> Bool {
        return getUIUserInterfaceIdiom() == .Phone
    }
    
    public class func isRetina() -> Bool {
        return UIScreen.mainScreen().scale >= 2.0
    }
    
    public class func isIPhone4OrLess() -> Bool {
        return isIPhone() && screenMaxLength() < 568.0
    }
    
    public class func isIPhone5() -> Bool {
        return isIPhone() && DLUtils.isEqual(screenMaxLength(), value2: 568.0)
    }
    
    public class func isIPhone6() -> Bool {
        return isIPhone() && DLUtils.isEqual(screenMaxLength(), value2: 667.0)
    }
    
    public class func isIPhone6Plus() -> Bool {
        return isIPhone() && DLUtils.isEqual(screenMaxLength(), value2: 736.0)
    }
    
    public class func getIOSVersion() -> Float {
        return (UIDevice.currentDevice().systemVersion as NSString).floatValue
    }
    
    public class func isIOS8orUp() -> Bool {
        return floor(getIOSVersion()) >= 8
    }


    // MARK: - Private
    
    private class func getUIUserInterfaceIdiom() -> UIUserInterfaceIdiom {
        return UIDevice.currentDevice().userInterfaceIdiom
    }
}
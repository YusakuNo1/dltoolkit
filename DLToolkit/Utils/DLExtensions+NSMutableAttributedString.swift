//
//  DLExtensions+NSMutableAttributedString.swift
//  DLToolkit
//
//  Created by David Wu on 6/04/2015.
//  Copyright (c) 2015 David Wu. All rights reserved.
//

import Foundation

public extension NSMutableAttributedString {
    
    public func replaceText(searchString: String, withAttributedString attributedString: NSMutableAttributedString) {
        let nsText = self.string as NSString
        let range = nsText.rangeOfString(searchString)
        self.replaceCharactersInRange(range, withAttributedString: attributedString)
    }
    
}

//
//  DLExtensions+UIColor.swift
//  DLToolkit
//
//  Created by David Wu on 6/04/2015.
//  Copyright (c) 2015 David Wu. All rights reserved.
//

import UIKit

public extension UIColor {
    
    public func toHexNSString() -> NSString {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        return NSString(format: "%02x%02x%02x", Int(red*255), Int(green*255), Int(blue*255))
    }
    
}

